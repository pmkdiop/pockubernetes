package com.sonatel.dsi.dif.poc.kub8.repository;

import com.sonatel.dsi.dif.poc.kub8.domain.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
