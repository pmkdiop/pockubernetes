/**
 * View Models used by Spring MVC REST controllers.
 */
package com.sonatel.dsi.dif.poc.kub8.web.rest.vm;
