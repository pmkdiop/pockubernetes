package com.sonatel.dsi.dif.poc.kub8.cucumber.stepdefs;

import com.sonatel.dsi.dif.poc.kub8.PockubernetesApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = PockubernetesApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
